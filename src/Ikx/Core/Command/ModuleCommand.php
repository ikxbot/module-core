<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Application;
use Ikx\Core\Model\Module;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\Table;

class ModuleCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        switch(strtoupper($this->params[0])) {
            case 'LIST':
                $modules = Module::getAll();
                $modulesList = [];
                foreach($modules as $name => $data) {
                    $modulesList[] = $name;
                }

                $this->msg($this->channel, __("Found %d modules: %s", count($modulesList), implode(", " , $modulesList)));
                $this->msg($this->channel, __("To get more info about the module, use \x02%s INFO <name>\x02", $this->command));
                break;
            case 'INFO':
                $this->infoAction($this->params[1] ?? '');
                break;
            default:
                $this->msg($this->channel, Format::bold(__('Module command')));

                $table = new Table();
                $table->addRow(__('Option'), __('Parameters'), __('Description'));
                $table->addRow('INFO', '<name>', __('Get information about a given module'));
                $table->addRow('LIST', '', __('List all available modules'));

                foreach ($table->get() as $row) {
                    $this->msg($this->channel, $row);
                }
                break;
        }
    }

    public function infoAction($modName) {
        $data = Module::get($modName);

        if ($data) {
            $commands = [];
            $events = [];

            foreach ($data['commands'] as $command => $actions) {
                foreach($actions as $c) {
                    if ($c['inChannel'] == true) {
                        $command = $this->network->get('prefix') . $command;
                    }
                    $commands[] = $command;
                }
            }

            foreach ($data['events'] as $event => $actions) {
                $events[] = (string) $event;
            }

            sort($events);
            sort($commands);

            $this->msg($this->channel, Format::bold($data['config']['name']) . ' - v' . $data['config']['version']);
            $this->msg($this->channel, __('Commands: %s', implode(', ', $commands)));
            $this->msg($this->channel, __('Events: %s', implode(', ', $events)));
        } else {
            $this->msg($this->channel, __('Module %s not found', Format::bold($modName)));
        }
    }

    public function describe()
    {
        return __("Module information command");
    }
}