<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Model\Command;
use Ikx\Core\Model\Module;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

/**
 * NOTICE event
 * Handles all commands sent to the bot using NOTICE commands
 * @package Ikx\Core\Event
 */
class NoticeEvent extends AbstractEvent implements EventInterface
{
    use MessagingTrait;

    private $command = '';
    private $fCommand = '';
    private $params = [];

    /**
     * Event executor
     */
    public function execute()
    {
        $first = substr($this->parts[3], 1);
        if (substr($first, 0, 1) == chr(1)) {
            $ctcpReply = strtoupper(substr($first, 1));
            if ($ctcpReply == 'VERSION') {
                /** @var User $user */
                $user = Network::getInstance()->getUser($this->nick);
                if ($user) {
                    $version = [];
                    for($i = 4; $i < count($this->parts); $i++) {
                        $version[] = $this->parts[$i];
                    }
                    $version = implode(' ', $version);

                    if (substr($version, -1, 1) == chr(1)) {
                        $version = substr($version, 0, -1);
                    }

                    $user->setVersion($version);
                }
            }
        }
    }
}