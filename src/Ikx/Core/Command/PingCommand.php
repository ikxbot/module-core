<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Utils\MessagingTrait;

class PingCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        $this->msg($this->channel, __('Pong %s', $this->nickname));
    }

    public function describe()
    {
        return __("Ping the bot check if it's still alive");
    }
}