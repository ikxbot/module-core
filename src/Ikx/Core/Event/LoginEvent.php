<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Entity\Network;
use Ikx\Core\Utils\MessagingTrait;

/**
 * Login Event
 * Listens on raw 001 - a raw 001 defines a succesful login to the server, so everyone that has to be done after connecting
 * to the server, can be done now
 * @package Ikx\Core\Event
 */
class LoginEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;

    /**
     * Event executor
     */
    public function execute()
    {
        $config = Network::getInstance()->get('config');
        if (isset($config['auth']) && isset($config['auth']['password'])) {
            if (isset($config['auth']['type']) && $config['auth']['type'] == 'NickServ') {
                $this->msg('NickServ', sprintf('ID %s', $config['auth']['password']));
            }
        }

        if (Network::getInstance()->get('logchan') != '') {
            $this->server->write('JOIN ' . Network::getInstance()->get('logchan'));
        }

        $channels = Network::getInstance()->get('channelCfg');
        foreach($channels as $channel) {
            $this->server->write('JOIN ' . $channel);
        }
    }
}