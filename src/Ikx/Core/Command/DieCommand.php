<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Entity\Network;
use Ikx\Core\Utils\MessagingTrait;

class DieCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        $this->server->write('QUIT :' . __('I\'m dead. Thanks, %s', $this->nickname));
        sleep(1);
        shell_exec('kill -9 ' . Network::getInstance()->get('pid'));
    }

    public function describe()
    {
        return __("Shutdown the bot (on this network)");
    }
}