<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

/**
 * Responds to the raw defining that the chosen nickname is already in use and selects a new nickname from the
 * configured alt nicks
 * @package Ikx\Core\Event
 */
class NickInUseEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        $altnicks = $this->network->get('altnick');
        $altnick = $altnicks[array_rand($altnicks, 1)];

        $this->server->write(sprintf("NICK %s", $altnick));
        $this->network->setCurrentNick($altnick);
    }
}