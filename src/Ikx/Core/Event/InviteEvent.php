<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

/**
 * Responds to the topic being sent to the client when a channel has been joined
 * @package Ikx\Core\Event
 */
class InviteEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        $channel = $this->parts[3];
        if (substr($channel, 0, 1) == ':') {
            $channel = substr($channel, 1);
        }

        $this->server->write(sprintf("JOIN %s", $channel));
        $this->server->log(sprintf("%s invited me to join %s", $this->nick, $channel));
    }
}