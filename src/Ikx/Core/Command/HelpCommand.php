<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Model\Module;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\Table;

class HelpCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        if (!isset($this->params[0])) {
            $modules = array_keys(Module::getAll());
            $this->msg($this->channel, __("Syntax: %s <module>", $this->network->get('prefix') . $this->command));
            $this->msg($this->channel, __("Available modules: %s", implode(', ', $modules)));

        } else {
            $moduleName = $this->params[0];
            $availableCommands = Module::getAvailableCommandsByModule($moduleName);

            if (!$availableCommands) {
                $this->msg($this->channel, __("Could not find module %s", $moduleName));
            } else {
                $table = new Table();
                $table->addRow(Format::bold(__('Command')), Format::bold(__('Lvl')), Format::bold(__('Info')));

                foreach($availableCommands as $command => $options) {
                    foreach ($options as $commandOption) {
                        $className = $commandOption['class'];
                        /** @var AbstractCommand $obj */
                        $obj = new $className($this->server, $this->network, $command, $this->params);

                        $commandFull = $this->network->get('prefix') . $command;

                        $table->addRow($commandFull, $commandOption['level'], $obj->describe());
                    }
                }

                if (count($table->get()) > 10) {
                    $this->msg($this->channel, __("Preventing flood, sending help in private."));

                    foreach($table->get() as $row) {
                        $this->notice($this->nickname, $row);
                    }
                } else {
                    foreach($table->get() as $row) {
                        $this->msg($this->channel, $row);
                    }
                }
            }
        }
    }

    public function describe()
    {
        return __("This help");
    }
}