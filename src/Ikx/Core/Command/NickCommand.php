<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class NickCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        if (isset($this->params[0])) {
            $this->server->write('NICK ' . $this->params[0]);
        } else {
            $this->msg($this->channel, __("%s: %s command requires at least one parameter, none given",
                Format::bold(__('ERROR')), $this->command));
        }
    }

    public function describe()
    {
        return __("Change my nickname");
    }
}