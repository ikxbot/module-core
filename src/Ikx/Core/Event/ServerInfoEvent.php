<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

/**
 * Server info event
 * Responds to raw 005 to set some server options
 * @package Ikx\Core\Event
 */
class ServerInfoEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        for($i = 3; $i < count($this->parts); $i++) {
            $ex = explode('=', $this->parts[$i]);

            switch($ex[0]) {
                case 'MAXCHANNELS':
                    $this->server->setMaxChannels($ex[1]);
                    break;
                case 'CHANLIMIT':
                    $this->server->setChanLimit($ex[1]);
                    break;
                case 'MAXNICKLEN':
                    $this->server->setMaxNicklen($ex[1]);
                    break;
                case 'NICKLEN':
                    $this->server->setNicklen($ex[1]);
                    break;
                case 'CHANNELLEN':
                    $this->server->setChannellen($ex[1]);
                    break;
                case 'TOPICLEN':
                    $this->server->setTopiclen($ex[1]);
                    break;
                case 'KICKLEN':
                    $this->server->setKicklen($ex[1]);
                    break;
                case 'AWAYLEN':
                    $this->server->setAwaylen($ex[1]);
                    break;
                case 'MAXTARGETS':
                    $this->server->setMaxtargets($ex[1]);
                    break;
                case 'CHANTYPES':
                    $this->server->setChantypes($ex[1]);
                    break;
                case 'PREFIX':
                    $this->server->setPrefix($ex[1]);
                    break;
            }
        }
    }
}