<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Utils\MessagingTrait;

class TimeCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        $this->msg($this->channel, strftime('%c') . ' (' . date_default_timezone_get() . ' / ' . setlocale(LC_ALL, 0) . ')');
    }

    public function describe()
    {
        return __("Fetch the current time");
    }
}