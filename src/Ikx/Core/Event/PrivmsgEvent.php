<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Model\Command;
use Ikx\Core\Model\Module;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

/**
 * PRIVMSG event
 * Handles all commands sent to the bot using PRIVMSG commands
 * @package Ikx\Core\Event
 */
class PrivmsgEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;

    public static $pids = [];
    public static $lastCtcpResponse = 0;
    private $command = '';
    private $fCommand = '';
    private $params = [];

    /**
     * Event executor
     */
    public function execute()
    {
        // Set the command
        $this->setCommand();

        // Fetch available user levels
        /** @var User $user */
        $user = $this->network->getUser($this->nick);
        if (!$user) { return; }
        $levels = $user->getLevel();

        if (substr($this->parts[3], 1, 1) == chr(1)) {
            $this->handleCtcp();
            return;
        }

        if ($this->channel != '') {
            // Private command
            if (($chanLevel = $user->getChannelLevel($this->channel)) > $levels) {
                $levels = $chanLevel;
            }
        }

        // Set the target and target type for
        $target = ($this->channel == '') ? $this->nick : $this->channel;
        $targetType = ($this->channel == '') ? 'notice' : 'msg';

        // Fetch available commands
        $availableCommands = Module::getAvailableCommands($this->channel != '');

        foreach ($availableCommands as $command => $options) {
            if (strtoupper($command) == $this->command) {
                foreach ($options as $commandOption) {

                    if ($commandOption['level'] <= $levels) {
                        $className = $commandOption['class'];
                        $start = microtime(true);
                        /** @var AbstractCommand $obj */
                        $obj = new $className($this->server, $this->network, $command, $this->params);

                        if ($obj->threaded === true && $this->server->ssl == false) {
                            $pid = null;

                            $pid = pcntl_fork();

                            if (!$pid || $pid == -1) {
                                $obj->setNickname($this->nick)
                                    ->setUsername($this->username)
                                    ->setAddress($this->address)
                                    ->setChannel($this->channel)
                                    ->setUser($user)
                                    ->run();

                                $time = microtime(true) - $start;
                                $this->server->log(sprintf('%s ran command %s in %.5fs [Threaded (%s)]', $this->nick, $this->fCommand, $time, $pid));

                                posix_kill(getmypid(), SIGQUIT);
                            } else {
                                self::$pids[] = [$pid => microtime(true)];
                            }
                        } else {
                            $obj->setNickname($this->nick)
                                ->setUsername($this->username)
                                ->setAddress($this->address)
                                ->setChannel($this->channel)
                                ->setUser($user)
                                ->run();

                            $time = microtime(true) - $start;
                            $this->server->log(sprintf('%s ran command %s in %.5fs', $this->nick, $this->fCommand, $time));
                        }
                    } else {
                        $this->$targetType($this->target,
                            sprintf("Access denied for command %s in module %s",
                                Format::bold($this->fCommand),
                                Format::bold($commandOption['module'])));

                        $this->server->log(sprintf('Denied access to %s for %s in module %s', $this->nick, $this->fCommand, $commandOption['module']));
                    }
                }
            }
        }

        if (rand(1, 15) == 3) {
            foreach (self::$pids as $pid => $time) {
                pcntl_waitpid($pid, $status, WNOHANG | WUNTRACED);
                unset(self::$pids[$pid]);
            }
        }
    }

    private function setCommand() {
        $paramsStart = 4;

        if ($this->channel == '') {
            // This is a private command
            $this->command = strtoupper(substr($this->parts[3], 1));
            $this->fCommand = $this->command;
        } else {
            // This is a channel command
            $firstWord = Format::strip(strtoupper(substr($this->parts[3], 1)));
            $myNick    = Network::getInstance()->get('currentNick');
            $prefix    = Network::getInstance()->get('prefix');

            $checkIsMyNick = substr($firstWord, 0, mb_strlen($myNick)) == strtoupper($myNick);

            if ($checkIsMyNick) {
                // First work is my own nick
                $this->command = strtoupper($this->parts[4]);
                $this->fCommand = $this->command;
                $paramsStart = 5;
            } elseif (substr($firstWord, 0, 1) == $prefix) {
                $this->fCommand = $firstWord;
                $this->command = substr($this->fCommand, 1);
            }
        }

        $this->command = Format::strip($this->command);

        for ($i = $paramsStart; $i < count($this->parts); $i++) {
            $this->params[] = $this->parts[$i];
        }
    }

    public function __execute()
    {
        if (substr($this->parts[3], 1, 1) == chr(1)) {
            $this->handleCtcp();
            return;
        }

        $me = $this->network->get('currentNick');
        if (strtoupper(mb_substr($this->parts[3], 1, mb_strlen($me))) == strtoupper($me)) {
            $command = $this->network->get('prefix') . strtoupper($this->parts[4]);

            $params = [];
            for($i = 5; $i < count($this->parts); $i++) {
                $params[] = $this->parts[$i];
            }
        }
        else {
            $command = strtoupper(mb_substr(Format::strip($this->parts[3]), 1));

            $params = [];
            for ($i = 4; $i < count($this->parts); $i++) {
                $params[] = $this->parts[$i];
            }
        }

        $fCommand = $command;
        if ($this->channel != '' && mb_substr($command, 0, 1) == $this->network->get('prefix')) {
            $command = mb_substr($command, 1);
        } else if($this->channel == '' && mb_substr($command, 0, 1) != $this->network->get('prefix')) {
            $fCommand = $this->network->get('prefix') . $fCommand;
        }

        $commands = Command::fetch();
        if (
            isset($commands[$command])
        ) {
            /** @var User $user */
            $user = $this->network->getUser($this->nick);
            $commandData = $commands[$command];
            if ($commandData['inChannel'] == ($this->channel != '') &&
                mb_substr($fCommand, 0, 1) == $this->network->get('prefix') &&
                (
                    $user->getLevel() >= $commandData['level'] ||
                    (
                        $this->channel != '' &&
                        $user->ison($this->channel) &&
                        $user->getChannelLevel($this->channel) > $commandData['level']
                    )
                )
            ) {
                $className = $commandData['class'];
                /** @var AbstractCommand $obj */
                $obj = new $className($this->server, $this->network, $command, $params);
                $obj->setNickname($this->nick)
                    ->setUsername($this->username)
                    ->setAddress($this->address)
                    ->setChannel($this->channel)
                    ->run();
            } else if($commandData['inChannel'] == ($this->channel != '') && mb_substr($fCommand, 0, 1) == $this->network->get('prefix')) {
                if ($this->channel != '') {
                    $this->msg($this->channel, "Access denied");
                } else {
                    $this->notice($this->nick, "Access denied");
                }
            }
        } else if($this->channel != '' && mb_substr($fCommand, 0, 1) == $this->network->get('prefix')) {
            $this->msg($this->channel, sprintf("Unknown command: %s",
                Format::bold($fCommand)));
        } else if($this->channel == '') {
            $this->notice($this->nick, sprintf("Unknown command: %s",
                Format::bold($command)));
        }
    }

    private function handleCtcp() {
        if (self::$lastCtcpResponse < (time() - 10)) {
            $req = strtoupper(substr($this->parts[3], 2));
            $req = str_replace(chr(1), '', $req);
            switch ($req) {
                case 'VERSION':
                    $osVersion = php_uname('n') . ' ' . php_uname('s') . ' ' . php_uname('r');
                    $this->ctcpreply($this->nick, 'VERSION', 'Ikx IRC bot v' . Application::VERSION . ' / ' . $osVersion . ' / PHP ' . PHP_VERSION);
                    break;
                case 'TIME':
                    $this->ctcpreply($this->nick, 'TIME', date('r'));
                    break;
                case 'PING':
                    //$this->ctcpreply($this->nick, 'PING', time());
                    $reply = str_replace(chr(1), '', $this->parts[4]);
                    $this->ctcpreply($this->nick, 'PING', $reply);
                    break;
                case 'FINGER':
                    $this->ctcpreply($this->nick, 'FINGER', 'Get your finger out of my USB port');
                    break;
            }

            self::$lastCtcpResponse = time();
        }
    }
}