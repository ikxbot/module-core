<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Entity\User;
use Ikx\Core\Utils\MessagingTrait;

/**
 * Class WhoEvent
 * @package Ikx\Core\Event
 */
class WhoEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;

    /**
     * Event executor
     */
    public function execute()
    {
        $channel  = $this->parts[3];
        $nickname = $this->parts[7];
        $username = $this->parts[4];
        $address  = $this->parts[5];
        $server   = $this->parts[6];
        $modes    = $this->parts[8];
        $realname = [];
        $user     = null;

        for($i = 10; $i < count($this->parts); $i++) {
            $realname[] = $this->parts[$i];
        }
        $realname = trim(implode(' ', $realname));

        if (!$this->network->getUser($nickname)) {
            /** @var User $user */
            $user = $this->network->createUser($nickname, $username, $address, $realname);

            // $this->ctcp($nickname, 'VERSION');
        }

        if (!$user) {
            $user = $this->network->getUser($nickname);
        }

        $channelObj = $this->network->getChannel($channel);
        if ($channelObj) {
            $user->addChannel($channelObj);
        }

        if (strstr($modes, 's')) {
            $user->setIsSecure();
        }

        if (stristr($modes, '*')) {
            $user->setChannelLevel($channel, User::LEVEL_OPER);
        } else if (stristr($modes, '?')) {
            $user->setChannelLevel($channel, User::LEVEL_OPER);
        }  else if (stristr($modes, '~')) {
            $user->setChannelLevel($channel, User::LEVEL_OWNER);
        } else if (stristr($modes, '&')) {
            $user->setChannelLevel($channel, User::LEVEL_ADMIN);
        } else if (stristr($modes, '@')) {
            $user->setChannelLevel($channel, User::LEVEL_OP);
        } else if (stristr($modes, '%')) {
            $user->setChannelLevel($channel, User::LEVEL_HALFOP);
        } else if (stristr($modes, '+')) {
            $user->setChannelLevel($channel, User::LEVEL_VOICE);
        } else {
            $user->setChannelLevel($channel, User::LEVEL_GUEST);
        }
    }
}