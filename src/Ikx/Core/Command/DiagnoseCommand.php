<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Application;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\Table;

class DiagnoseCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        $this->msg($this->channel, 'Bot diagnosing will be messaged to ' . Format::bold($this->nickname));

        $memoryUsage = number_format(memory_get_usage() / 1024 / 1024, 5);
        $memoryPeakUsage = number_format(memory_get_peak_usage() / 1024 / 1024, 5);

        $table = new Table();
        $table->addRow(Format::bold(__('PHP version')), PHP_VERSION);
        $table->addRow(Format::bold(__('Ikx version')), Application::VERSION);
        $table->addRow(Format::bold(__('Database name')), $this->network->getDbName());
        $table->addRow(Format::bold(__('Loaded classes')), count(get_declared_classes()));
        $table->addRow(Format::bold(__('Loaded interfaces')), count(get_declared_interfaces()));
        $table->addRow(Format::bold(__('Loaded traits')), count(get_declared_traits()));
        $table->addRow(Format::bold(__('Memory usage')), $memoryUsage . 'MB');
        $table->addRow(Format::bold(__('Memory peak usage')), $memoryPeakUsage . 'MB');

        foreach($table->get() as $row) {
            $this->notice($this->nickname, $row);
        }
    }

    public function describe()
    {
        return __("Diagnose the bot's state");
    }
}