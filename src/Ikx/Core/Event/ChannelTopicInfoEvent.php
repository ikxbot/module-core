<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

/**
 * Responds to the raw telling who set the topic and when
 * @package Ikx\Core\Event
 */
class ChannelTopicInfoEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        $topicSetBy = $this->parts[4];
        $topicSetAt = $this->parts[5];

        $this->network->getChannel($this->parts[3])
                      ->setTopicSetBy($topicSetBy)
                      ->setTopicSetAt($topicSetAt);
    }
}