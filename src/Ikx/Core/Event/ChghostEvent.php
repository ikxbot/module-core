<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Entity\User;

/**
 * Responds to the raw telling who set the topic and when
 * @package Ikx\Core\Event
 */
class ChghostEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        $newIdent = $this->parts[2];
        $newAddress  = $this->parts[3];

        /** @var User $user */
        $user = $this->network->getUser($this->nick);

        if ($user) {
            if (
                $user->getAddress() != $newAddress ||
                $user->getUsername() != $newIdent
            ) {
                $oldAddress = $user->getAddress();
                $oldIdent = $user->getUsername();
                $user->setAddress($newAddress);
                $user->setUsername($newIdent);
                $this->server->log(sprintf('Host for %s has changed to %s@%s (was %s@%s)',
                    $this->nick, $newIdent, $newAddress, $oldIdent, $oldAddress));
            }
        }
    }
}