<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Utils\MessagingTrait;

/**
 * Join event
 * @package Ikx\Core\Event
 */
class JoinEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;

    /**
     * Event executor
     */
    public function execute()
    {
        if ($this->nick == $this->network->get('currentNick')) {
            $this->network->createChannel($this->target);

            $this->server->write(sprintf('MODE %s', $this->target));
            $this->server->write(sprintf('WHO %s', $this->target));
        } else {
            $this->server->write(sprintf('WHO %s', $this->nick));
        }
    }
}