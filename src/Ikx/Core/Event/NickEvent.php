<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

/**
 * Nick change event
 * @package Ikx\Core\Event
 */
class NickEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        $newNick = $this->parts[2];
        if (substr($newNick, 0 ,1) == ':') {
            $newNick = substr($newNick, 1);
        }

        if ($this->nick == $this->network->get('currentNick')) {
            $this->network->setCurrentNick($newNick);
        } else {
            $this->network->changeUser($this->nick, $newNick);
        }
    }
}