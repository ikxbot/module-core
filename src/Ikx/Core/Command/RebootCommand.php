<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Console\RunCommand;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\Server;
use Ikx\Core\Utils\MessagingTrait;

class RebootCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;
    public $threaded = true;

    public function run() {
        $this->msg($this->channel, "Not implemented. Bye.");

        /** @var Network $network */
        foreach(RunCommand::$networks as $network) {
            /** @var Server $server */
//            $server = $network->get('server');
//            $server->write("QUIT :Reboot requested by %s", $this->nickname);
        }

        sleep(1);
        $pidPath = sprintf('%s/Ikx.pid', IKX_ROOT);
        $ikxPath = $_SERVER['PHP_SELF'];
        $phpExec = $_SERVER['_'];
        $pid = trim(file_get_contents($pidPath));

        $pids = str_replace("\n", " ", $pid);
        shell_exec(sprintf('nohup %s -f %s -- run', $phpExec, $ikxPath));
        shell_exec(sprintf('kill -1 %s; kill -9 %s', $pids, $pids));
        foreach(explode("\n", $pid) as $id) {
            posix_kill($id, 1);
        }
    }

    public function describe()
    {
        return __("Reboot.");
    }
}