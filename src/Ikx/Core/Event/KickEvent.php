<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Entity\Network;
use Ikx\Core\Utils\MessagingTrait;

/**
 * PingEvent (responds to PING)
 * @package Ikx\Core\Event
 */
class KickEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;

    /**
     * Event executor
     */
    public function execute()
    {
        if ($this->parts[3] == Network::getInstance()->get('currentNick')) {
            $this->server->write(sprintf('JOIN %s', $this->parts[2]));
            $this->msg($this->channel, sprintf("Thanks for kicking me %s!", $this->nick));
            sleep(2);
            $this->server->write(sprintf("KICK %s %s :My revenge is sweet ;)", $this->parts[2], $this->nick));
        }
    }
}