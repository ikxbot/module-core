<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Entity\Network;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class PartCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        if (isset($this->params[0])) {
            $first = substr($this->params[0], 0, 1);
            if (!in_array($first, $this->server->getChantypes())) {
                $this->msg($this->channel, __("%s: Invalid channel specified. Allowed channel types: %s",
                    Format::bold(__('ERROR')), implode(', ', $this->server->getChantypes())));
            } else if(!Network::getInstance()->getChannel($this->params[0])) {
                $this->msg($this->channel, __("%s: I'm not on channel %s",
                    Format::bold(__('ERROR')), $this->params[0]));
            } else {
                $this->server->write('PART ' . join(' ', $this->params));
            }
        } else {
            $this->server->write('PART ' . $this->channel);
        }
    }

    public function describe()
    {
        return __("Leave a channel");
    }
}