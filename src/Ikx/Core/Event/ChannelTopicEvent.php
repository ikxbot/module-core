<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

/**
 * Responds to the topic being sent to the client when a channel has been joined
 * @package Ikx\Core\Event
 */
class ChannelTopicEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        $topicParts = [];
        for($i = 4; $i < count($this->parts); $i++) {
            $topicParts[] = $this->parts[$i];
        }
        $topic = substr(implode(' ', $topicParts), 1);
        $this->network->getChannel($this->parts[3])->setTopic($topic);
    }
}