<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\Server;
use Ikx\Core\Entity\User;

/**
 * Abstract command handler
 * @package Ikx\Core\Command
 */
abstract class AbstractCommand {
    /** @var string Command being called */
    protected $command = '';
    /** @var array Command params */
    protected $params = [];
    /** @var Server Server entity */
    protected $server;
    /** @var Network Network entity */
    protected $network;
    /** @var string Channel name */
    protected $channel = '';
    /** @var string Nickname */
    protected $nickname = '';
    /** @var string Username */
    protected $username = '';
    /** @var string User address */
    protected $address = '';

    /** @var User User requesting the command */
    protected $user;

    public $threaded = false;

    /**
     * Command constructor
     * @param Server $server        Server instance
     * @param Network $network      Network insance
     * @param string $command       Command being ran
     * @param array $params         Command params
     */
    public function __construct(
        Server $server,
        Network $network,
        string $command,
        array $params
    ) {
        $this->server = $server;
        $this->network = $network;
        $this->command = $command;
        $this->params = $params;
    }

    public function setNickname($nickname) {
        $this->nickname = $nickname;
        return $this;
    }

    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

    public function setChannel($channel) {
        $this->channel = $channel;
        return $this;
    }

    public function setUser(User $user) {
        $this->user = $user;
        return $this;
    }
}