<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Application;
use Ikx\Core\Utils\MessagingTrait;

class StfuCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        if (Application::config()->get('options.queue.enabled')) {
            $this->network->getDb()->queue->drop();
            $this->msg($this->channel, __("Queue cleaned. I'll be quiet."));
        } else {
            $this->msg($this->channel, __("Queue is disabled, I can't shut up. Sorry :("));
        }
    }

    public function describe()
    {
        return __("SHUT THE FUCK UP");
    }
}