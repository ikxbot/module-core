<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Command;

use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\Table;

class IgnoreCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        switch(strtoupper($this->params[0])) {
            case 'ADD':
                $this->add();
                break;
            case 'DEL':
                $this->del();
                break;
            case 'LIST':
                $this->listAction();
                break;
            default:
                $this->msg($this->channel, Format::bold(__('IRC ignore list')));

                $table = new Table();
                $table->addRow(__('Option'), __('Parameters'), __('Description'));
                $table->addRow('ADD', '<nick|address>', __('Add a user to the ignore list (will ignore based on address when a nick is given)'));
                $table->addRow('DEL', '<nick|address>', __('Remove a nick or address from the ignore list'));
                $table->addRow('LIST', '', __('Display the ignore list'));

                foreach($table->get() as $row) {
                    $this->msg($this->channel, $row);
                }
                break;
        }
    }

    public function describe()
    {
        return __("Handle ignores");
    }

    private function add() {
        if (count($this->params) > 1) {
            $ignoreCollection = $this->network->getDb()->ignore;
            $nickname = $this->params[1];

            /** @var User $user */
            $user = $this->network->getUser($nickname);
            $thisUser = $this->network->getUser($this->nickname);
            if ($user) {
                if (
                    $user->getLevel() > $thisUser->getLevel()/* ||
                    $user->getChannelLevel($this->channel) > $thisUser->getChannelLevel($this->channel) ||
                    $user->getLevel() < $thisUser->getChannelLevel($this->channel)*/ ||
                    strtolower($nickname) == strtolower(Network::getInstance()->get('currentNick'))
                ) {
                    $this->msg($this->channel, __("You can't add a user with a higher level than yours to the ignore list."));
                } else {
                    $nickname = $user->getNickname();
                    $address = $user->getAddress();

                    $ignoreCollection->insertOne([
                        'nickname' => $nickname,
                        'address' => $address
                    ]);

                    $this->msg($this->channel, __('User has been added to ignore list.'));
                }
            } else {
                $this->msg($this->channel, __('Unable to find user %s', Format::bold($nickname)));
            }
        } else {
            $this->msg($this->channel, __('Syntax: %s %s <nick|address>', $this->command, 'ADD'));
        }
    }

    private function del() {
        if (count($this->params) > 1) {
            $ignoreCollection = $this->network->getDb()->ignore;
            $ignoreCollection->findOneAndDelete(['nickname' => $this->params[1]]);
            $ignoreCollection->findOneAndDelete(['address' => $this->params[1]]);

            $this->msg($this->channel, __('Record removed.'));
        } else {
            $this->msg($this->channel, __('Syntax: %s %s <nick|address>', $this->command, 'DEL'));
        }
    }

    private function listAction() {
        $ignoreCollection = $this->network->getDb()->ignore;

        if ($ignoreCollection->countDocuments() > 0) {
            $table = new Table();
            $table->addRow(Format::bold(__('Nickname')), Format::bold(__('Address')));
            foreach ($ignoreCollection->find() as $record) {
                $table->addRow($record->nickname, $record->address);
            }

            foreach ($table->get() as $row) {
                $this->msg($this->channel, $row);
            }
        } else {
            $this->msg($this->channel, __('Ignore list is empty!'));
        }
    }
}