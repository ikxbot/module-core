<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Core\Event;

use Ikx\Core\Entity\User;

/**
 * Mode handler event
 * @package Ikx\Core\Event
 */
class ModeEvent extends AbstractEvent implements EventInterface {
    /**
     * Event executor
     */
    public function execute()
    {
        if ($this->channel != '') {
            $userStatusChanged = false;

            for($i = 3; $i < count($this->parts); $i++) {
                /** @var User $user */
                $user = $this->network->getUser($this->parts[$i]);
                if ($user) {
                    $this->server->write(sprintf('WHO %s', $user->getNickname()));
                    $userStatusChanged = true;
                }
            }

            if ($userStatusChanged) {
                $this->server->write(sprintf('WHO %s', $this->channel));
            }
        }
    }
}